#include <LowPower.h>

#define PIN_sensor                      2                   // Reed sensor
#define PIN_buzzer                      3
#define WAIT_TIME_BEFORE_BUZZING_MS     ((long) 5 * 1000)   // wait seconds before making buzz signal

bool buzzerState;
long startTimeDoorOpen;
bool isDoorOpened;

void setup()
{
    Serial.begin(115200);

    pinMode(PIN_sensor,INPUT);
    digitalWrite(PIN_sensor, HIGH);

    pinMode(PIN_buzzer, OUTPUT);
    digitalWrite(PIN_buzzer, HIGH);

    pinMode(5,INPUT);
    digitalWrite(5, LOW);
    pinMode(6,INPUT);
    digitalWrite(6, LOW);
    pinMode(7,INPUT);
    digitalWrite(7, LOW); 
    pinMode(8,INPUT);
    digitalWrite(8, LOW); 

    buzzerState = false;
}

// the method that is called on interrupt on pin 2
void pin2_isr()
{
}

void makeSound()
{
    if(isDoorOpened)
    {
        long periodOpenedDoor = millis() - startTimeDoorOpen;
        if(periodOpenedDoor <= WAIT_TIME_BEFORE_BUZZING_MS)
        {
            // just wait
        }
        else if(periodOpenedDoor <= 10000 + WAIT_TIME_BEFORE_BUZZING_MS)
        {
            digitalWrite(PIN_buzzer, LOW);
            delay(1);
            digitalWrite(PIN_buzzer, HIGH);
            delay(1000);
        }
        else if(periodOpenedDoor <= 15000 + WAIT_TIME_BEFORE_BUZZING_MS)
        {
            digitalWrite(PIN_buzzer, LOW);
            delay(15);
            digitalWrite(PIN_buzzer, HIGH);
            delay(1000);
        }
        else if(periodOpenedDoor <= 20000 + WAIT_TIME_BEFORE_BUZZING_MS)
        {
            digitalWrite(PIN_buzzer, LOW);
            delay(100);
            digitalWrite(PIN_buzzer, HIGH);
            delay(1000);
        }
        else
        {
            digitalWrite(PIN_buzzer, LOW);
            delay(200);
            digitalWrite(PIN_buzzer, HIGH);
            delay(1000);
        }
    }
}

void loop ()
{ 
    bool prevState = isDoorOpened;
    isDoorOpened = digitalRead(PIN_sensor) == HIGH;
     delay(30);

    if(prevState == false && isDoorOpened == true)
    {
        startTimeDoorOpen = millis();
        delay(30);
    }

    if(isDoorOpened == true)
    {
        makeSound();
    }
    else
    {
        attachInterrupt(0, pin2_isr, HIGH);
        LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF); 
        detachInterrupt(0);
    }
}
