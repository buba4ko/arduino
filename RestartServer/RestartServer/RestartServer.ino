#include <EtherCard.h>
#include <LowPower.h>
#include "constants.h"
#include "common.h"
#include "eepromHelper.h"
#include "customSleep.h"

extern GlobalSettings globalSettings;

#define STATIC 1  // set to 1 to disable DHCP (adjust myip/gwip values below)

#if STATIC
// ethernet interface ip address
static byte myip[] = MY_IP;
// gateway ip address
static byte gwip[] = GATEWAY_IP;
static byte dnsip[] = DNS_IP;
static byte mask[] = MY_MASK;
#endif

// ethernet interface mac address, must be unique on the LAN
static byte mymac[] = MY_MAC;

byte Ethernet::buffer[600];
static uint32_t timer;


bool hasPing;
int noPingCounter;
long lastTimeRestart;
long lastTimePing = 0;
//bool hasPingToGateway;

const char page2_start[] =
"HTTP/1.0 200 OK\r\n"
"Content-Type: text/html\r\n"
"\r\n"
"<html>"
  "<head><title>"
    "Restart Counter"
  "</title></head>"
  "<body>"
  "Times server restarted: %d"
  "</body>"
"</html>"
;

int getIntLength(int number)
{
    int length = 0;
    if (number < 0)
    {
        length++;
        number = -number;
    }
    if(number >= 0) length++;
    if(number >= 10) length++;
    if(number >= 100) length++;
    if(number >= 1000) length++;
    if(number >= 10000) length++;
//    if(number >= 100000) length++;
//    if(number >= 1000000) length++;
//    if(number >= 10000000) length++;
    return length;
}


void setup()
{    
    Serial.begin(56000);
    Serial.println("---------- start -------------- ");

    pinMode(PIN_relay, OUTPUT);       
    digitalWrite(PIN_relay, 1);

    hasPing = true;
//    hasPingToGateway = true;
    noPingCounter = 0;
    lastTimeRestart = -PERIOD_WAIT_BEFORE_SECOND_RESTART_MS; // make it negative, to allow imemdiate restart if needed

    checkEEPROMIsInitialized();
    initEEPROMData();

    Serial.print("settingsValue_restartCounter = ");
    Serial.println(globalSettings.settingsValue_restartCounter);
    delay(20);

    if (ether.begin(sizeof Ethernet::buffer, mymac, 10) == 0)
    {
        Serial.println(F("Failed to access Ethernet controller"));
    }

    #if STATIC
        ether.staticSetup(myip, gwip, dnsip, mask);
    #else
        if (!ether.dhcpSetup())
            Serial.println("DHCP failed");
    #endif

    ether.printIp("IP:  ", ether.myip);
    ether.printIp("GW:  ", ether.gwip);  
    ether.printIp("DNS: ", ether.dnsip);  
    ether.printIp("Mask: ", ether.netmask);  

    ether.parseIp(ether.hisip, IP_TO_PING);
}

bool pingServer(bool isGateway = false)
{
    bool hasPing = false;
    uint8_t  (*ipToPing)[4];

//    if(isGateway)
//    {
//        ether.parseIp(ether.hisip, IP_GATEWAY);
//    }

    timer = micros();

    if(isGateway)
    {
        Serial.println("pinging gateway");
        ipToPing = &(ether.gwip);
    }
    else
    {
        ipToPing = &(ether.hisip);
    }
    ether.clientIcmpRequest(*ipToPing);

    ether.printIp("Pinging: ", *ipToPing);
    
//ether.clientIcmpRequest(ether.hisip);

    while(micros() - timer <= (uint32_t) PERIOD_WAIT_PING_BACK_MS * 1000) // 3s
    {
        word len = ether.packetReceive(); // go receive new packets
        word pos = ether.packetLoop(len); // respond to incoming pings
  
        // report whenever a reply to our outgoing ping comes back
        if (len > 0 && ether.packetLoopIcmpCheckReply(*ipToPing)) {
		//if (len > 0 && ether.packetLoopIcmpCheckReply(ether.hisip)) {
            Serial.print("  ");
            Serial.print((micros() - timer) * 0.001, 1);
            Serial.println(" ms");
            hasPing = true;
            break;
        }
    }

  if(hasPing == false)
  {
    Serial.println(" no ping");
  }

  return hasPing;
}

void restartCurrent()
{
    Serial.println("restarting current...");
    delay(20);
    digitalWrite(PIN_relay, 0);
    delay(PERIOD_KEEP_NO_POWER); 

    Serial.println("current started again");
    delay(20);

    digitalWrite(PIN_relay, 1);
    delay(PERIOD_WAIT_AFTER_RESTART); 

    noPingCounter = 0;
    lastTimeRestart = myMillis();
    lastTimePing = millis();
	globalSettings.lastRestartMillis = millis();
    globalSettings.settingsValue_restartCounter = globalSettings.settingsValue_restartCounter + 1;
    saveIntToEEPROM(EEPROM_INDEX_RESTART_COUNTER, globalSettings.settingsValue_restartCounter);
    
    Serial.print("settingsValue_restartCounter = ");
    Serial.println(globalSettings.settingsValue_restartCounter);

    Serial.println("can loop again");
    delay(20);
}

void loop()
{
    if(millis() - lastTimeRestart < PERIOD_WAIT_BEFORE_SECOND_RESTART_MS)
    {
        Serial.println(F("have to wait 30 mins before restart again !"));
        delay(DELAY_AFTER_NOT_ALLOWED_RESTART_S * 1000);
        Serial.print(F("Remaining: "));
        long remainingSecs = (PERIOD_WAIT_BEFORE_SECOND_RESTART_MS - (millis() - lastTimeRestart)) / 1000;
        Serial.print(remainingSecs / 60);
        Serial.print(F(" min "));
        Serial.print(remainingSecs % 60);
        Serial.println(F(" s"));
    }
    else
    {
        bool isTimeToPing;

		isTimeToPing = hasPing ? 
			millis() - lastTimePing > DELAY_AFTER_SUCCESS_PING_S * 1000  
				:  millis() - lastTimePing > DELAY_AFTER_FAILED_PING_S * 1000;

        if(isTimeToPing)
        {
            lastTimePing = millis();

//            if(hasPingToGateway == false)
//            {
//                hasPingToGateway = pingServer(true);
//            }
//            else
//            {
                hasPing = pingServer();
                if(hasPing)
                {
        //            Serial.println("-- has ping -- ");
        //            delay(20);
                    noPingCounter = 0;
            //        gotoDeepSleep(DELAY_AFTER_SUCCESS_PING_S);           // sleep 1 min
                //    delay(1000 * DELAY_AFTER_SUCCESS_PING_S);           // sleep 1 min
                }
                else
                {
                    noPingCounter++;

    //                Serial.print(" no ping: ");
                    Serial.println(noPingCounter);
                    delay(20);
                    if(noPingCounter == NUMBER_ATTEMPTS_PINGS)
                    {
//                        hasPingToGateway = pingServer(true);
//                        if(hasPingToGateway)
//                        {
                            restartCurrent();
//                        }
//                        else
//                        {
//                            noPingCounter = 0; 
//                        }
                    }
                    else
                    {
                   //     delay(1000 * DELAY_AFTER_FAILED_PING_S);           // sleep 30 sec
            //            gotoDeepSleep(DELAY_AFTER_FAILED_PING_S);           // sleep 30 sec
                    }
                }
//            }
        }

        if (ether.packetLoop(ether.packetReceive())) 
		{
      //      Serial.println("1111111111");
            char buffer[200];
            sprintf(buffer, page2_start, globalSettings.settingsValue_restartCounter);
            int str_len = sizeof (page2_start)  - 2  + getIntLength(globalSettings.settingsValue_restartCounter);
        

    //        char buffer[400];
      //      String mas = String(page2_start) + String("Times Restarted: 11");// + String(globalSettings.settingsValue_restartCounter) +  String(page2_end);
         //   int str_len = buffer.length() + 1;
          //  Serial.println(str_len);
    //        Serial.println(mas);

           // mas.toCharArray(buffer, str_len);

            memcpy(ether.tcpOffset(), buffer, str_len);
            ether.httpServerReply(str_len - 1);
        }
    }

    delay(50);
}