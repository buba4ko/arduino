#include <Arduino.h>

//#include "timer.h"
#include "common.h"
#include "constants.h"

long millisDiscrepancy = 0;
long freezeTime = 0;
long freezePeriod = 0;

long myMillis()
{
    if(freezePeriod > 0)
    {
        Sprintln(F("!!! SEVERE ERROR !!! You called myMillis() before defrosting millis !"));
        delay(10000);
    }

    // in case we overrun the long size, clear the discreapncy
    long result = millis() + millisDiscrepancy;
    if (result < 0)
    {
        millisDiscrepancy = 0;
        result = millis();
    }
    return result;
}


void freezeMillis(long periodMs)
{
    if(freezePeriod > 0)
    {
        Sprintln(F("!!! SEVERE ERROR !!! You called freezeMillis() before defrosting millis !"));
        delay(10000);
    }
    freezeTime = millis();
    freezePeriod = periodMs;
}

void defrostMillis()
{
    long discr = freezeTime + freezePeriod - millis();
  //  Serial.println(discr);
    millisDiscrepancy += discr;
    freezePeriod = 0;
}

