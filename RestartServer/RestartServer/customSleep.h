#ifndef CUSTOM_SLEEP_H
#define CUSTOM_SLEEP_H



void pin2_isr()
{}

void gotoDeepSleep(unsigned int sleepTimeS)
{
 //   Serial.print(F("deep sleep for s.."));
 //   Serial.println(sleepTimeS);
 //   delay(10);
 
    byte sleepFrameSize = 1;// 4 = SLEEP_4S or 8
    unsigned int sleepCounter = 1 + ((sleepTimeS - 1) / sleepFrameSize);
   // Serial.print("sleepCounter = ");
  //  Serial.println(sleepCounter);
 //   delay(30);


    // is it OK when it is here, but not in the loop?
  //  attachInterrupt(0, pin2_isr, LOW);
  //  attachInterrupt(1, pin2_isr, LOW);

    for(uint8_t i = 0; i < sleepCounter; i++)
    {
    //    Serial.println(String("pillow..."));
   //     delay(30);

        /*wdt_enable((long)sleepTimeS * 1000);
        WDTCSR |= (1 << WDIE);    
        lowPowerBodOff(SLEEP_MODE_PWR_DOWN);
        */
        freezeMillis(sleepFrameSize*1000);
        LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF); 
        defrostMillis();

       

  //      Serial.println("breath...");
    //    delay(20);
    }

    // !!! has to be immediately after the wake up !!!
    sleep_disable();
 //   detachInterrupt(0);
  //  detachInterrupt(1);

    // natural wake up - reset working timer
//    if(globalButtons.pinInterruptFlag == InterruptStates_DeepSleep)
    {
  //      globalButtons.renew_working_timer_for_deep_sleep();
    }



 //   Serial.println(F("waking up..."));
  //  delay(10);

}

#endif // CUSTOM_SLEEP_H
