#include <Arduino.h>
#include <EEPROM.h>
#include "constants.h"
#include "eepromHelper.h"
//#include "measuredValues.h"
//#include "display.h" // if you remove it, you save flash space


GlobalSettings globalSettings;
//extern MeasuredValues statistics;
// extern PressureLogger globalsPressureLogger;


// --------------------------------------------------------------------------------------
// EEPROM helper functions - BEGIN
void saveIntToEEPROM(int index, int value)
{
    byte * ptr = (byte*)&value;
    EEPROM.write(index, *ptr);
    EEPROM.write(index + 1, *(ptr + 1));
}

int readIntFromEEPROM(int index)
{
    int result;

    byte* ptr = (byte*) &result;
    *ptr = EEPROM.read(index);
    *(ptr+1) = EEPROM.read(index + 1);

    return result;
}
/*
void saveLongToEEPROM(int index, long value)
{
    byte * ptr = (byte*)&value;
    EEPROM.write(index, *ptr);
    EEPROM.write(index + 1, *(ptr + 1));
    EEPROM.write(index + 2, *(ptr + 2));
    EEPROM.write(index + 3, *(ptr + 3));
}

long readLongFromEEPROM(int index)
{
    long result;

    byte* ptr = (byte*) &result;
    *ptr = EEPROM.read(index);
    *(ptr+1) = EEPROM.read(index + 1);
    *(ptr+2) = EEPROM.read(index + 2);
    *(ptr+3) = EEPROM.read(index + 3);

    return result;
}
// EEPROM helper functions - END
// --------------------------------------------------------------------------------------
*/
/*
// writes to EEPROM and updates the global value
void writeSetting(int eepromIndex, int eepromValue)
{
    switch (eepromIndex)
    {
        case EEPROM_INDEX_RESTART_COUNTER:
            globalSettings.settingsValue_restartCounter = eepromValue;
            break;
    }
    
    EEPROM.write(eepromIndex, eepromValue);
}

byte readSetting(int eepromIndex)
{
    return EEPROM.read(eepromIndex);
}
*/

void initEEPROMData()
{
    globalSettings.settingsValue_restartCounter = readIntFromEEPROM(EEPROM_INDEX_RESTART_COUNTER);
}

void resetEEPROMData()
{
    // write all config values to their init values
    globalSettings.settingsValue_restartCounter = DEFAULT_SETTING_VALUE_RESET_COUNTER;

    saveIntToEEPROM(EEPROM_INDEX_RESTART_COUNTER, globalSettings.settingsValue_restartCounter);                
}

bool checkEEPROMIsInitialized()
{
    if(EEPROM.read(EEPROM_INDEX_PROGRAM_MARKER) != EEPROM_VALUE_PROGRAM_MARKER)
    {
        EEPROM.write(EEPROM_INDEX_PROGRAM_MARKER, EEPROM_VALUE_PROGRAM_MARKER);
        resetEEPROMData();
        return false;
    }
    return true;
}



