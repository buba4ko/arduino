﻿#ifndef EEPROM_HELPER_H
#define EEPROM_HELPER_H

#include <EEPROM.h>

#define EEPROM_VALUE_PROGRAM_MARKER                         ((byte) 0x21)
#define EEPROM_INDEX_PROGRAM_MARKER                         ((byte) 600)
#define EEPROM_INDEX_RESTART_COUNTER                        ((byte) 601) // 2 bytes

class GlobalSettings
{
public:
    int settingsValue_restartCounter;
};


void saveIntToEEPROM(int index, int value);
int readIntFromEEPROM(int index);
//void writeSetting(int eepromIndex, byte eepromValue);
//byte readSetting(int eepromIndex);

void initEEPROMData();
void resetEEPROMData();

bool checkEEPROMIsInitialized();


#endif // EEPROM_HELPER_H
