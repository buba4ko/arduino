//#include <EtherCard.h>
//#include <EEPROM.h>

#include <ESP8266WiFi.h>
#include <ESP8266Ping.h>
//#include <LowPower.h>
#include "constants.h"
#include "common.h"
#include "eepromHelper.h"
//#include "customSleep.h"

extern GlobalSettings globalSettings;

enum states currentState;

#define STATIC 0  // set to 1 to disable DHCP (adjust myip/gwip values below)

#if STATIC
// ethernet interface ip address
static byte myip[] = MY_IP;
// gateway ip address
static byte gwip[] = GATEWAY_IP;
static byte dnsip[] = DNS_IP;
static byte mask[] = MY_MASK;
#endif

// ethernet interface mac address, must be unique on the LAN
static byte mymac[] = MY_MAC;

//byte Ethernet::buffer[600];
static uint32_t timer;


bool hasPing;
int noPingCounter;
unsigned long lastTimeRestart;
unsigned long lastTimePing = 0;
//bool hasPingToGateway;

const char page2_start[] =
"HTTP/1.0 200 OK\r\n"
"Content-Type: text/html\r\n"
"\r\n"
"<html>"
  "<head><title>"
    "Restart Counter"
  "</title></head>"
  "<body>"
  "Times server restarted: %d\r\n"
  "Last restarts: %s"
  "</body>"
"</html>"
;

// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(80);

int getIntLength(int number)
{
    int length = 0;
    if (number < 0)
    {
        length++;
        number = -number;
    }
    if(number >= 0) length++;
    if(number >= 10) length++;
    if(number >= 100) length++;
    if(number >= 1000) length++;
    if(number >= 10000) length++;
//    if(number >= 100000) length++;
//    if(number >= 1000000) length++;
//    if(number >= 10000000) length++;
    return length;
}


void setup()
{    
    Serial.begin(115200);
    Serial.println("---------- start -------------- ");

   // pinMode(PIN_relay, OUTPUT);       
    //digitalWrite(PIN_relay, 1);

	// Connect to WiFi network
	Serial.println();
	Serial.println();
	Serial.print("Connecting to ");
	Serial.println(NETWORK_SSID);
	delay(80);

	WiFi.begin(NETWORK_SSID, NETWORK_PASSWORD);

	while (WiFi.status() != WL_CONNECTED) {
		delay(1000);
		Serial.print(".");
	}
	Serial.println("");
	Serial.println("WiFi connected");

	// Start the server
	server.begin();
	Serial.println("Server started");

	// Print the IP address
	Serial.println(WiFi.localIP());

	/*
    hasPing = true;
//    hasPingToGateway = true;
    noPingCounter = 0;
    lastTimeRestart = -PERIOD_WAIT_BEFORE_SECOND_RESTART_MS; // make it negative, to allow imemdiate restart if needed

    checkEEPROMIsInitialized();
    initEEPROMData();

    Serial.print("settingsValue_restartCounter = ");
    Serial.println(globalSettings.settingsValue_restartCounter);
    delay(20);

    if (ether.begin(sizeof Ethernet::buffer, mymac, 10) == 0)
    {
        Serial.println(F("Failed to access Ethernet controller"));
    }

    #if STATIC
        ether.staticSetup(myip, gwip, dnsip, mask);
    #else
        if (!ether.dhcpSetup())
            Serial.println("DHCP failed");
    #endif

    ether.printIp("IP:  ", ether.myip);
    ether.printIp("GW:  ", ether.gwip);  
    ether.printIp("DNS: ", ether.dnsip);  
    ether.printIp("Mask: ", ether.netmask);  

    ether.parseIp(ether.hisip, IP_TO_PING);
	*/

	EEPROM.begin(512);  //Initialize EEPROM
	checkEEPROMIsInitialized();
	initEEPROMData();

	currentState = state_normal;
}

void HelloArduinoTeam(WiFiClient& client)
{
	long mils = millis();
	int mil = mils % 1000;
	int sec = (mils / 1000) % 60;
	int min = (mils / (60 * 1000)) % 60;
	int hours = (mils / (60 * 60 * 1000)) % 60;
	String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\nHello Arduino Team ";
	s += "\r\n<br/>" + String(hours) + " hours " + min + " mins " + sec + " sec " + mil + " ms";
	s += "</html>\n";
	client.print(s);
	delay(3);
	Serial.println("Client disonnected");
	client.stop();
}

bool pingServer(bool isGateway = false)
{
    bool hasPing = false;
    uint8_t  (*ipToPing)[4];

    timer = micros();

 /*   if(isGateway)
    {
        Serial.println("pinging gateway");
        ipToPing = &(ether.gwip);
    }
    else
    {
        ipToPing = &(ether.hisip);
    }
    ether.clientIcmpRequest(*ipToPing);

    ether.printIp("Pinging: ", *ipToPing);*/
    

  //  while(micros() - timer <= (uint32_t) PERIOD_WAIT_PING_BACK_MS * 1000) // 3s
  //  {
  //      word len = ether.packetReceive(); // go receive new packets
  //      word pos = ether.packetLoop(len); // respond to incoming pings
  //
  //      // report whenever a reply to our outgoing ping comes back
  //      if (len > 0 && ether.packetLoopIcmpCheckReply(*ipToPing)) {
		////if (len > 0 && ether.packetLoopIcmpCheckReply(ether.hisip)) {
  //          Serial.print("  ");
  //          Serial.print((micros() - timer) * 0.001, 1);
  //          Serial.println(" ms");
  //          hasPing = true;
  //          break;
  //      }
  //  }

	Serial.print("pinging...");
	Serial.print(IP_TO_PING);
	
	hasPing = Ping.ping(IP_TO_PING);

  if(hasPing == false)
  {
    Serial.print(" no ping ");
	Serial.println(noPingCounter);

	
  }
  else
  {
	  Serial.println(" OK");
  }

  return hasPing;
}

void restartCurrent()
{
    Serial.println("restarting current...");
    delay(20);
    digitalWrite(PIN_relay, 0);
    delay(PERIOD_KEEP_NO_POWER); 

    Serial.println("current started again");
    delay(20);

    digitalWrite(PIN_relay, 1);
    delay(PERIOD_WAIT_AFTER_RESTART); 

    noPingCounter = 0;
    lastTimeRestart = myMillis();
    lastTimePing = millis();
	addRestartTime(millis());
    globalSettings.settingsValue_restartCounter = globalSettings.settingsValue_restartCounter + 1;
    saveIntToEEPROM(EEPROM_INDEX_RESTART_COUNTER, globalSettings.settingsValue_restartCounter);
    
    Serial.print("settingsValue_restartCounter = ");
    Serial.println(globalSettings.settingsValue_restartCounter);

    Serial.println("can loop again");
    delay(20);
}

WiFiClient client;
void handleGetReguest()
{

	if (WiFi.status() != WL_CONNECTED) {
		Serial.println("[loop] no wifi");
		delay(1000);
	}

	// Check if a client has connected
	client = server.available();
	if (!client) {
		//  delay(10);
		return;
	}

	// Wait until the client sends some data
	Serial.println();
	Serial.println("new client");
	delay(3);
	long counter = 0;
	while (!client.available()) {
		counter++;
		delay(3);
		if (counter > (long)10 * 1000)
		{
			Serial.println("endless loop detected");
			break;
		}
	}

	// Read the first line of the request
	String req = client.readStringUntil('\r');
	Serial.println(req);
	client.flush();

	// Match the request
	int val;
	if (req.indexOf("/gpio/0") != -1)
		val = 0;
	else if (req.indexOf("/gpio/1") != -1)
		val = 1;
	else if (req.indexOf("/favicon.ico") != -1)
	{
		// ignore the favicon.ico request
		Serial.println("ignoring the favicon.ico");
		delay(3);
		return;
	}
	else {
		client.flush();
		HelloArduinoTeam(client);
		//Serial.println("invalid request");
		//client.stop();
		return;
	}

	// Set GPIO2 according to the request
	digitalWrite(2, val);

	client.flush();

	// Prepare the response
	String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\nGPIO is now ";
	s += (val) ? "high" : "low";
	s += "</html>\n";

	// Send the response to the client
	client.print(s);
	delay(1);
	Serial.println("Client disonnected");
	client.stop();

	// The client will actually be disconnected 
	// when the function returns and 'client' object is detroyed


}


void performPing()
{
	lastTimePing = millis();
	hasPing = pingServer();

	registerPingResult();
}

void registerPingResult()
{
	if (hasPing)
	{
		noPingCounter = 0;
	}
	else
	{
		noPingCounter++;
	}
}

void setState(states newState)
{
	switch (newState)
	{
		case state_normal:
			break;
		case state_noping:
			break;
		case state_router_restarted:
			break;
		case state_should_restart_router:
			break;
		default:
			Serial.print("unknown state in setState()! : ");
			Serial.println(newState);
			delay(10000);
			break;
	}
	currentState = newState;
}

void proceedCurrentState()
{
	bool isTimeToPing;

	switch (currentState)
	{
		case state_normal:
			isTimeToPing = millis() - lastTimePing > DELAY_AFTER_SUCCESS_PING_S * 1000;
			if (isTimeToPing)
			{
				performPing();
				if (hasPing == false)
				{
					setState(state_noping);
				}
			}
		break;

		case state_noping:
			isTimeToPing = millis() - lastTimePing > DELAY_AFTER_FAILED_PING_S * 1000;
			if (isTimeToPing)
			{
				performPing();
				if (hasPing == false && noPingCounter >= NUMBER_ATTEMPTS_PINGS)
				{
					setState(state_should_restart_router);
				}
			}
		break;

		case state_should_restart_router:
			restartCurrent();
			setState(state_router_restarted);
		break;

		case state_router_restarted:
			if (millis() - lastTimeRestart < PERIOD_WAIT_BEFORE_SECOND_RESTART_S * 1000)
			{
				Serial.println(F("have to wait 30 mins before restart again !"));
				delay(DELAY_AFTER_NOT_ALLOWED_RESTART_S * 1000);
				Serial.print(F("Remaining: "));
				long remainingSecs = PERIOD_WAIT_BEFORE_SECOND_RESTART_S - (millis() - lastTimeRestart) / 1000;
				Serial.print(remainingSecs / 60);
				Serial.print(F(" min "));
				Serial.print(remainingSecs % 60);
				Serial.println(F(" s"));
			}
			else
			{
				currentState = state_normal;
			}
		break;

		default:
			Serial.print("unknown state in proceedCurrentState()! : ");
			Serial.println(currentState);
			delay(10000);
			break;
		}
}

void loop()
{
	handleGetReguest();
	delay(20);

	proceedCurrentState();
	delay(20);

	getLastRestartsAsString();
	delay(300);
}


void loop2()
{
	handleGetReguest();
	delay(20);

	getLastRestartsAsString();
	delay(300);
	//return;

	if (millis() - lastTimeRestart < PERIOD_WAIT_BEFORE_SECOND_RESTART_S * 1000)
	{
		Serial.println(F("have to wait 30 mins before restart again !"));
		delay(DELAY_AFTER_NOT_ALLOWED_RESTART_S * 1000);
		Serial.print(F("Remaining: "));
		long remainingSecs = (PERIOD_WAIT_BEFORE_SECOND_RESTART_S * 1000 - (millis() - lastTimeRestart)) / 1000;
		Serial.print(remainingSecs / 60);
		Serial.print(F(" min "));
		Serial.print(remainingSecs % 60);
		Serial.println(F(" s"));
	}
	else
	{
		bool isTimeToPing;

		isTimeToPing = hasPing ?
			millis() - lastTimePing > DELAY_AFTER_SUCCESS_PING_S * 1000
			:  millis() - lastTimePing > DELAY_AFTER_FAILED_PING_S * 1000;

		if (isTimeToPing)
		{
			lastTimePing = millis();

			//            if(hasPingToGateway == false)
			//            {
			//                hasPingToGateway = pingServer(true);
			//            }
			//            else
			//            {
			hasPing = pingServer();
			if (hasPing)
			{
				//            Serial.println("-- has ping -- ");
				//            delay(20);
				noPingCounter = 0;
				//        gotoDeepSleep(DELAY_AFTER_SUCCESS_PING_S);           // sleep 1 min
					//    delay(1000 * DELAY_AFTER_SUCCESS_PING_S);           // sleep 1 min
			}
			else
			{
				noPingCounter++;

				//                Serial.print(" no ping: ");
				Serial.println(noPingCounter);
				delay(20);
				if (noPingCounter == NUMBER_ATTEMPTS_PINGS)
				{
					//                        hasPingToGateway = pingServer(true);
					//                        if(hasPingToGateway)
					//                        {
					restartCurrent();
					//                        }
					//                        else
					//                        {
					//                            noPingCounter = 0; 
					//                        }
				}
				else
				{
					//     delay(1000 * DELAY_AFTER_FAILED_PING_S);           // sleep 30 sec
			 //            gotoDeepSleep(DELAY_AFTER_FAILED_PING_S);           // sleep 30 sec
				}
			}
			//            }
		}

		//      if (ether.packetLoop(ether.packetReceive())) 
			  //{
		//          char buffer[200];
			  //	const char * restartedTimesInfo = getLastRestartsAsString();
		//          sprintf(buffer, page2_start, globalSettings.settingsValue_restartCounter, restartedTimesInfo);
		//          int str_len = sizeof (page2_start)  - 2  + getIntLength(globalSettings.settingsValue_restartCounter);

		//          memcpy(ether.tcpOffset(), buffer, str_len);
		//          ether.httpServerReply(str_len - 1);
		//      }
	}

	delay(50);
}
