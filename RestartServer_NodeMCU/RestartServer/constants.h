#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "Arduino.h"

#define PIN_relay                           7              

#define PERIOD_WAIT_PING_BACK_MS            ((long) 2000)               // 2000
#define PERIOD_KEEP_NO_POWER                ((long) 8000)               // 15000
#define PERIOD_WAIT_AFTER_RESTART           ((long) 3000)               // 30000
#define PERIOD_WAIT_BEFORE_SECOND_RESTART_S ((long) 20)    // 30 min

#define DELAY_AFTER_SUCCESS_PING_S          ((int) 3)           // 1 min
#define NUMBER_ATTEMPTS_PINGS               ((int) 3)          // 3
#define DELAY_AFTER_FAILED_PING_S           ((int) 4)           // 30s
#define DELAY_AFTER_FAILED_PING_TO_GATE_S   ((int) 3)           // 30s
#define DELAY_AFTER_NOT_ALLOWED_RESTART_S   ((int) 3)           // 3s


// -------- cvetomila ----------- 
//#define MY_IP                               { 192,168,1,155 }
//#define MY_MASK                             { 255,255,255,0 }
//#define MY_MAC                              { 0x74,0x69,0x69,0x2D,0x30,0x31 }
//#define GATEWAY_IP                          { 192,168,1,1}
//#define IP_GATEWAY                          "192.168.1.1"
//#define DNS_IP                              { 192,168,1,1}
//#define IP_TO_PING                          "192.168.1.101" // 243, 165

// -------- office -----------
#define MY_IP                               { 100,102,0,155 }
#define MY_MASK                             { 255,255,255,0 }
#define MY_MAC                              { 0x74,0x69,0x69,0x2D,0x30,0x31 }
#define GATEWAY_IP                          { 100,102,0,1}
#define IP_GATEWAY                          "100.102.0.1"
#define DNS_IP                              { 100,102,0,1}
#define IP_TO_PING                          "192.168.1.167" //"100.102.0.244" // 243, 165


#define NETWORK_SSID						"raikova" //"sbnd"; // PrimeHolding
#define NETWORK_PASSWORD					"rozovoflamingo"

//242   165
#define ARRAY_SIZE(array)                   (sizeof(array)/sizeof(array[0]))

#define DEFAULT_SETTING_VALUE_RESET_COUNTER ((int) 0)


#define Sprint(a)                           Serial.print(a)
#define Sprintln(a)                         Serial.println(a)

//enum states
//{
//	state_normal_wait_time_to_ping,
//	state_normal_time_to_ping,
//	state_noping_waiting_to_ping_again,
//	state_noping_time_to_ping_again,
//	state_noping_waiting_min_time_to_restart,
//	state_noping_time_to_restart,
//	state_noping_just_restarted,
//	state_noping_canNotRestart_restarted_soon
//};
enum states
{
	state_normal,
	state_noping,
	state_should_restart_router,
	state_router_restarted
};

#endif // CONSTANTS_H
