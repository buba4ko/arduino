//#include <Arduino.h>

#include "constants.h"
#include "eepromHelper.h"
//#include "measuredValues.h"
//#include "display.h" // if you remove it, you save flash space


GlobalSettings globalSettings;
//extern MeasuredValues statistics;
// extern PressureLogger globalsPressureLogger;


// --------------------------------------------------------------------------------------
// EEPROM helper functions - BEGIN
void saveIntToEEPROM(int index, int value)
{
    byte * ptr = (byte*)&value;
    EEPROM.write(index, *ptr);
    EEPROM.write(index + 1, *(ptr + 1));
	EEPROM.commit();
}

int readIntFromEEPROM(int index)
{
    int result;

    byte* ptr = (byte*) &result;
    *ptr = EEPROM.read(index);
    *(ptr+1) = EEPROM.read(index + 1);

    return result;
}


void initEEPROMData()
{
    globalSettings.settingsValue_restartCounter = readIntFromEEPROM(EEPROM_INDEX_RESTART_COUNTER);
	int size = sizeof(globalSettings);
	byte* obj = (byte*)(void*) &globalSettings;
	for(int i = 0; i < size; i++)
	{
		*(obj + i) = EEPROM.read(i + EEPROM_INDEX_RESTART_COUNTER);
	}
}

void resetEEPROMData()
{
    // write all config values to their init values
    globalSettings.settingsValue_restartCounter = DEFAULT_SETTING_VALUE_RESET_COUNTER;

	globalSettings.lastRestartMillisSize = 0;

	for(byte i=0; i < ARRAY_SIZE(globalSettings.lastRestartMillis); i++)
	{
		globalSettings.lastRestartMillis[i] = 0;
	}
	

    saveIntToEEPROM(EEPROM_INDEX_RESTART_COUNTER, globalSettings.settingsValue_restartCounter);                
	saveToEepRom();
}

bool checkEEPROMIsInitialized()
{
	Serial.println("11");
	delay(20);
    if(EEPROM.read(EEPROM_INDEX_PROGRAM_MARKER) != EEPROM_VALUE_PROGRAM_MARKER)
    {
		Serial.println("22");
		delay(20);

        EEPROM.write(EEPROM_INDEX_PROGRAM_MARKER, EEPROM_VALUE_PROGRAM_MARKER);
        resetEEPROMData();
		EEPROM.commit();
        return false;
    }
    return true;
}

void saveToEepRom()
{
	byte size = sizeof(globalSettings);
	byte* obj = (byte*) (void*) &globalSettings;

    for(int i = 0; i < size; i++)
    {
        EEPROM.write(i + EEPROM_INDEX_RESTART_COUNTER, *(obj + i));
    }
	EEPROM.commit();
}



void addRestartTime(unsigned long restartMillis)
{
	// remove the last one if filled 
	if(globalSettings.lastRestartMillisSize == EEPROM_KEEP_LAST_RESTARTS_COUNT)
	{
		globalSettings.lastRestartMillisSize--;
	}
	// move the items with one positions to the right
	for(int i = globalSettings.lastRestartMillisSize - 1; i >= 0 ; i--)
	{
		globalSettings.lastRestartMillis[i+1] = globalSettings.lastRestartMillis[i];
	}
	// increase the size
	globalSettings.lastRestartMillisSize++;

	// add it at the beginning
	globalSettings.lastRestartMillis[0] = restartMillis;

	saveToEepRom();
}

const char* getLastRestartsAsString()
{
	char buffer[120] = "none";
	char temp[30];
	int bufferIndex = 0;

	Serial.print("globalSettings.lastRestartMillisSize = ");
	Serial.println(globalSettings.lastRestartMillisSize);
	for(int i = 0; i < globalSettings.lastRestartMillisSize; i++)
	{
			//Serial.print("globalSettings.lastRestartMillis[i]");
	Serial.println(globalSettings.lastRestartMillis[i]);

		unsigned long allSeconds=globalSettings.lastRestartMillis[i] / 1000;
		int runHours = allSeconds / 3600;
		int secsRemaining = allSeconds % 3600;
		int runMinutes = secsRemaining / 60;
		int runSeconds = secsRemaining % 60;

		sprintf(temp,"%02d:%02d:%02d ;   . ",runHours,runMinutes,runSeconds);
	//	Serial.print(temp);
		for(char* c = temp; *c != 0; c++)
		{
		//	Serial.print("c=");
		//	Serial.println(*c);
			buffer[bufferIndex] = *c;
			bufferIndex++;
		}
	}
	buffer[bufferIndex] = 0;

//	Serial.println(1111);
	Serial.println("buffer = ");
	//Serial.println(buffer);
		//Serial.println("bufferIndex = ");
	Serial.println(bufferIndex);
//	Serial.println(22222);
	return buffer;
}


