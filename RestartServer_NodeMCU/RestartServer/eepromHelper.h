﻿#ifndef EEPROM_HELPER_H
#define EEPROM_HELPER_H

#include <EEPROM.h>

#define EEPROM_VALUE_PROGRAM_MARKER                         ((byte) 0x22)
#define EEPROM_INDEX_PROGRAM_MARKER                         ((byte) 100)
#define EEPROM_INDEX_RESTART_COUNTER                        ((byte) 101) // 2 bytes

#define EEPROM_KEEP_LAST_RESTARTS_COUNT                     ((byte) 10)

class GlobalSettings
{
public:
    int settingsValue_restartCounter;
	unsigned long lastRestartMillis[EEPROM_KEEP_LAST_RESTARTS_COUNT];
	byte lastRestartMillisSize;
};


void saveIntToEEPROM(int index, int value);
int readIntFromEEPROM(int index);
//void writeSetting(int eepromIndex, byte eepromValue);
//byte readSetting(int eepromIndex);

void initEEPROMData();
void resetEEPROMData();

bool checkEEPROMIsInitialized();

void saveToEepRom();
void addRestartTime(unsigned long restartMillis);
const char* getLastRestartsAsString();


#endif // EEPROM_HELPER_H
