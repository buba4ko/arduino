 #include <avr/sleep.h>
//#include <LowPower.h>

const byte PIN_button1 = 2;
const byte PIN_button4 = 3;
const byte PIN_led = 13;
const byte PIN_led_big = 5;

//bool button1State = false;
//bool button4State = false;

enum AllButtonStates{
	AllButtonState_NoPressed = 0,
	AllButtonState_Btn1_ShortReleased = 1,
	AllButtonState_Btn1_LongPressed = 2,
	AllButtonState_Btn4_ShortReleased = 3,
	AllButtonState_Btn4_LongPressed = 4,
	AllButtonState_Btn3_ShortReleased = 5,
	AllButtonState_Btn3_LongPressed = 6,
	AllButtonState_Btn1_Btn4_Pressed = 7
};

enum ButtonStates{
	ButtonState_Unknown = 100,
	ButtonState_NoPressed = 0,
	ButtonState_Pressed = 1,
	ButtonState_ShortReleased = 2,
	ButtonState_LongPressed = 3
};

class global 
{
public:

	bool isHandledButton1Press;
	//ButtonStates button4PrevState;
	ButtonStates button1State;
	ButtonStates button4State;
	AllButtonStates allButtonStates;
	long timeWhenButton1WasPressed;
	long timeWhenButton4WasPressed;

public:
	global()
	{
		button1State = ButtonState_NoPressed;
		button4State = ButtonState_NoPressed;
		isHandledButton1Press = false;
	}
/*	bool isButton1JustShortPressed(){
		return button1State == ButtonState_ShortReleased && button1PrevState != ButtonState_NoPressed;
	}
	bool isButton1JustLongPressed(){
		return button1State == ButtonState_LongPressed && button1PrevState != ButtonState_Pressed;
	}
	*/
	ButtonStates ReadButtonsState()
	{
		//byte button1NowPressed;
		//byte button4NowPressed;
		bool button1NowPressed = digitalRead(PIN_button1) == LOW; // !
		bool button4NowPressed = digitalRead(PIN_button4) == LOW; // !
		delay(20);


		doSomething(&button1State, button1NowPressed, &timeWhenButton1WasPressed);
		doSomething(&button4State, button4NowPressed, &timeWhenButton4WasPressed);

	//	Serial.println("button1State = " + String(button1State));
	//	delay(400);


		if(button1State == ButtonState_Pressed && button4State == ButtonState_Pressed)
		{
			allButtonStates = AllButtonState_Btn1_Btn4_Pressed;
		}
		else if(button1State == ButtonState_ShortReleased)
		{
		//	Serial.println("aaaaaa= ");
		//	delay(20);
			allButtonStates = AllButtonState_Btn1_ShortReleased;
		}
		else if(button1State == ButtonState_LongPressed)
		{
				//	Serial.println("AllButtonState_Btn1_LongPressed = ");
			allButtonStates = AllButtonState_Btn1_LongPressed;
		}
		else if(button4State == ButtonState_ShortReleased)
		{
			allButtonStates = AllButtonState_Btn4_ShortReleased;
		}
		else if(button4State == ButtonState_LongPressed)
		{
			allButtonStates = AllButtonState_Btn4_LongPressed;
		}
		else if(button1State == ButtonState_Pressed || button4State == ButtonState_Pressed)
		{
			allButtonStates =AllButtonState_NoPressed;
		//	Serial.println("a11");
			//allButtonStates = AllButtonState_NoPressed;
		}

///		Serial.println("allButtonStates = " + String(allButtonStates));

		/*
		if(button1State == ButtonState_NoPressed && /*timeWhenButton1WasPressed == 0 && * / button1NowPressed)
		{
			// is pressed for first time
//			Serial.println("press at: " + String(millis()));
			timeWhenButton1WasPressed = millis();
			button1State = ButtonState_Pressed;
			isHandledButton1Press = false;
		}
		else if(timeWhenButton1WasPressed > 0 && (millis() - timeWhenButton1WasPressed > 700) && button1NowPressed)
		{
			//Serial.println(millis());
			// long click detected
			//timeWhenButton1WasPressed = 0;
			button1State = ButtonState_LongPressed;
			
		}		
		else if(/*timeWhenButton1WasPressed > 0 &&* / button1State == ButtonState_Pressed && button1NowPressed == false)
		{
			// short released
			timeWhenButton1WasPressed = 0;
			button1State = ButtonState_ShortReleased;
		}
		else if(button1NowPressed == false)
		{
			 my - timeWhenButton1WasPressed = 0;
			button1State = ButtonState_NoPressed;
		}
		*/

		//if(button1NowPressed || button4NowPressed)
	//	{
			renew_working_timer();
	//	}
	}

						bool old_state;
	void doSomething(ButtonStates* buttonState, bool buttonIsNowPressed, long* timeWhenButtonWasPressed)
	{
/*		if(old_state != buttonIsNowPressed)
		{
			old_state = buttonIsNowPressed;
			Serial.println("-- state is changed!  " + String(buttonIsNowPressed) );
			delay(40);
		}
	*/

	//	Serial.println("-- buttonState " + String(*buttonState) );
	//	Serial.println("timeWhenButtonWasPressed " + String(*timeWhenButtonWasPressed) );
	//	Serial.println("buttonIsNowPressed " + String(buttonIsNowPressed) );
		//Serial.println("isButtonStatePressed " + String(isButtonStatePressed) );
		
			//delay(100);

		//Serial.println("isButtonStateNoPressed" + String(isButtonStateNoPressed));
		//Serial.println("press at: " + String(*timeWhenButtonWasPressed));
		//ButtonStates buttonState = ButtonState_Unknown;
		if(*buttonState == ButtonState_NoPressed && /*timeWhenButton1WasPressed == 0 && */ buttonIsNowPressed)
		{
			// is pressed for first time
			//Serial.println("press at: " + String(millis()));
			*timeWhenButtonWasPressed = millis();
			*buttonState = ButtonState_Pressed;
			isHandledButton1Press = false;
		}
		else if(*timeWhenButtonWasPressed > 0 && (millis() - *timeWhenButtonWasPressed > 700) && buttonIsNowPressed)
		{
		//	Serial.println(millis());
			// long click detected
			//timeWhenButton1WasPressed = 0;
			*buttonState = ButtonState_LongPressed;
			
		}		
		else if(/*timeWhenButton1WasPressed > 0 &&*/ *buttonState == ButtonState_Pressed && buttonIsNowPressed == false)
		{
//			Serial.println("short released !!!");
			// short released
			*timeWhenButtonWasPressed = 0;
			*buttonState = ButtonState_ShortReleased;
		}
		else if(buttonIsNowPressed == false)
		{
		//	Serial.println("clear btn state!!!");
			*buttonState = ButtonState_NoPressed;
			*timeWhenButtonWasPressed = 0;
		}
		else
		{
		//	*buttonState = ButtonState_Unknown;
		}

	//return buttonState;
	}
};

void setup() {        
	Serial.begin(9600);

	pinMode(PIN_button1, INPUT);    
	pinMode(PIN_button4, INPUT);  
	pinMode(PIN_led, OUTPUT); 
	pinMode(PIN_led_big, OUTPUT); 

	digitalWrite(PIN_button1, HIGH);
	digitalWrite(PIN_button4, HIGH);
	digitalWrite(PIN_led_big, LOW);
}

//byte button1;
//byte button4;
//byte pin2_interrupt_flag = 1;
long working_time_ms = millis();
global globals;


void pin2_isr()
{
//  sleep_disable();
//  detachInterrupt(0);
//  pin2_interrupt_flag = 0;
  
  

}

inline void renew_working_timer()
{
	working_time_ms = millis();
}

void MonitorButtons()
{
	//Serial.println("globals.allButtonStates = " + String(globals.allButtonStates));
	//delay(200);

	if(globals.allButtonStates == AllButtonState_Btn1_Btn4_Pressed && globals.isHandledButton1Press == false)
	{
		globals.isHandledButton1Press = true;
		Serial.println("$$$ reset requested ...");
		delay(20);
	}
	if(globals.allButtonStates == AllButtonState_Btn1_ShortReleased && globals.isHandledButton1Press == false)
	{
		globals.isHandledButton1Press = true;
		Serial.println("$$$ short pressed 1 ...");
		delay(20);
	}
	else if(globals.allButtonStates == AllButtonState_Btn1_LongPressed   && globals.isHandledButton1Press == false)
	{
		globals.isHandledButton1Press = true;
		Serial.println("$$$ long pressed 1 ...");
		delay(20);
	}
	if(globals.allButtonStates == AllButtonState_Btn4_ShortReleased && globals.isHandledButton1Press == false)
	{
		globals.isHandledButton1Press = true;
		Serial.println("$$$ short pressed 4 ...");
		delay(20);
	}
	else if(globals.allButtonStates == AllButtonState_Btn4_LongPressed  && globals.isHandledButton1Press == false)
	{
		globals.isHandledButton1Press = true;
		Serial.println("$$$ long pressed 4 ...");
		delay(20);
	}

	if(globals.button1State == ButtonState_Pressed)
	{
		digitalWrite(PIN_led_big, HIGH);
		//delay(1000);
		//digitalWrite(PIN_led_big, LOW);
	}
	else
	{
		digitalWrite(PIN_led_big, LOW);
	}

}

void sleepNow() {  
	
	// turn off LED not to drain power
	digitalWrite(PIN_led_big, LOW);

    sleep_enable();          // enables the sleep bit in the mcucr register  
    attachInterrupt(0,pin2_isr, LOW); // use interrupt 0 (pin 2) and run function  
	attachInterrupt(1,pin2_isr, LOW); // use interrupt 1 (pin 3) and run function  
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);   // sleep mode is set here  
	cli();
	sleep_bod_disable();
	sei();
	sleep_cpu();
	sleep_mode();            // here the device is actually put to sleep!!  

    // THE PROGRAM CONTINUES FROM HERE AFTER WAKING UP  
    sleep_disable();         // first thing after waking from sleep: disable sleep...  
    detachInterrupt(0);      // disables interrupt 0 on pin 2 so the wakeUpNow code will not be executed during normal running time.  
	detachInterrupt(1);
/*
			sleep_enable();
		attachInterrupt(0, pin2_isr, LOW);
		attachInterrupt(1, pin2_isr, LOW);
		// 0, 1, or many lines of code here 
		set_sleep_mode(SLEEP_MODE_PWR_DOWN);
		cli();
		sleep_bod_disable();
		sei();
		sleep_cpu();
		sleep_mode();  
		// wake up here 
		sleep_disable();
		detachInterrupt(0);
		detachInterrupt(1);
		*/
		renew_working_timer();
		globals.ReadButtonsState();

}  

void loop() {


	globals.ReadButtonsState();

	//Serial.println(globals.button1State);
//	delay(50);

	MonitorButtons();
	
	
	bool gotoSleep = false;

	if(/*pin2_interrupt_flag == 1 || */ millis() - working_time_ms > 8000)
	{
		gotoSleep = true;
		Serial.println("going to sleep ...");
		delay(30);
	}

	if(gotoSleep)
	{
//		attachInterrupt(0, pin2_isr, LOW);
	//	attachInterrupt(1, pin2_isr, LOW);
//		 LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF); 
	/*	sleep_enable();
		attachInterrupt(0, pin2_isr, LOW);
		attachInterrupt(1, pin2_isr, LOW);
		// 0, 1, or many lines of code here 
		set_sleep_mode(SLEEP_MODE_PWR_DOWN);
		cli();
		sleep_bod_disable();
		sei();
		sleep_cpu();
		sleep_mode();  
		// wake up here 
		sleep_disable();
		detachInterrupt(0);
		detachInterrupt(1);
		working_time_ms = millis();
		globals.ReadButtonsState();
		*/
		sleepNow();
		Serial.println("...waking up");
		delay(10);
	}
	/*
	Serial.println("working");
	button1 = digitalRead(PIN_button1);
	button4 = digitalRead(PIN_button4);
	delay(10);

	if(button1State == false && button1 == LOW)
	{
		button1State = true;
		Serial.println(String("Button 1 is pressed !"));
		digitalWrite(PIN_led, HIGH);
		delay(100);             
		digitalWrite(PIN_led, LOW); 
	}
	if(button1 == HIGH)
	{
		button1State = false;
	}

	if(button4State == false && button4 == LOW)
	{
		button4State = true;
		Serial.println(String("Button 4 is pressed !"));
		digitalWrite(PIN_led, HIGH);
		delay(100);             
		digitalWrite(PIN_led, LOW); 
	}
	if(button4 == HIGH)
	{
		button4State = false;
	}

	delay(20);
	*/
}
