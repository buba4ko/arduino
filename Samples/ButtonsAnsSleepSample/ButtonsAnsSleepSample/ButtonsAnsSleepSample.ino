 #include <avr/sleep.h>
//#include <LowPower.h>

const byte PIN_button1 = 2;
const byte PIN_button4 = 3;
const byte PIN_button3 = 4;
const byte PIN_led = 13;
const byte PIN_led_big = 5;

#define LOND_PRESS_TIME				((int) 700)		/* time in ms to wait to detect long click */
#define TIME_BEFORE_GOING_TO_SLEEP	((int) 8000)	/* time in ms to wait before going to sleep when no button is pressed */


enum AllButtonStates{
	AllButtonState_NoPressed = 0,
	AllButtonState_Btn1_ShortReleased = 1,
	AllButtonState_Btn1_LongPressed = 2,
	AllButtonState_Btn4_ShortReleased = 3,
	AllButtonState_Btn4_LongPressed = 4,
	AllButtonState_Btn3_ShortReleased = 5,
	AllButtonState_Btn3_LongPressed = 6,
	AllButtonState_Btn1_Btn4_Pressed = 7,
	AllButtonState_Btn1_Btn3_Btn4_Pressed = 12
};

enum ButtonStates{
	ButtonState_NoPressed = 0,
	ButtonState_Pressed = 1,
	ButtonState_ShortReleased = 2,
	ButtonState_LongPressed = 3
};

class global 
{
public:
	// general global variables
	long working_time_ms;

	// button properties
	bool isHandledButtonPress;
	ButtonStates button1State;
	ButtonStates button3State;
	ButtonStates button4State;
	AllButtonStates allButtonStates;
	long timeWhenButton1WasPressed;
	long timeWhenButton3WasPressed;
	long timeWhenButton4WasPressed;

public:
	global()
	{
		working_time_ms = millis();

		button1State = ButtonState_NoPressed;
		button3State = ButtonState_NoPressed;
		button4State = ButtonState_NoPressed;
		isHandledButtonPress = false;
	}

	ButtonStates ReadButtonsState()
	{
		bool button1NowPressed = digitalRead(PIN_button1) == LOW; // !
		bool button3NowPressed = digitalRead(PIN_button3) == LOW; // !
		bool button4NowPressed = digitalRead(PIN_button4) == LOW; // !
		delay(20);

		ReadSingleButtonState(&button1State, button1NowPressed, &timeWhenButton1WasPressed);
		ReadSingleButtonState(&button3State, button3NowPressed, &timeWhenButton3WasPressed);
		ReadSingleButtonState(&button4State, button4NowPressed, &timeWhenButton4WasPressed);

//		Serial.println("--button3State = " + String(button3State));
	//	Serial.println("button3NowPressed = " + String(button3NowPressed));
		//delay(400);

		if(button1State == ButtonState_NoPressed && button3State == ButtonState_NoPressed && button4State == ButtonState_NoPressed)
		{
			// do nothing - just ignore
		}
		else if(button1State == ButtonState_Pressed && button3State == ButtonState_Pressed && button4State == ButtonState_Pressed)
		{
			allButtonStates = AllButtonState_Btn1_Btn3_Btn4_Pressed;
		}
		else if(button1State == ButtonState_Pressed && button4State == ButtonState_Pressed)
		{
			allButtonStates = AllButtonState_Btn1_Btn4_Pressed;
		}
		else if(button1State == ButtonState_ShortReleased)
		{
			allButtonStates = AllButtonState_Btn1_ShortReleased;
		}
		else if(button1State == ButtonState_LongPressed)
		{
			allButtonStates = AllButtonState_Btn1_LongPressed;
		}
		else if(button3State == ButtonState_ShortReleased)
		{
			allButtonStates = AllButtonState_Btn3_ShortReleased;
		}
		else if(button3State == ButtonState_LongPressed)
		{
			allButtonStates = AllButtonState_Btn3_LongPressed;
		}
		else if(button4State == ButtonState_ShortReleased)
		{
			allButtonStates = AllButtonState_Btn4_ShortReleased;
		}
		else if(button4State == ButtonState_LongPressed)
		{
			allButtonStates = AllButtonState_Btn4_LongPressed;
		}
		else if(button1State == ButtonState_Pressed || button3State == ButtonState_Pressed || button4State == ButtonState_Pressed)
		{
			allButtonStates = AllButtonState_NoPressed;
		}

///		Serial.println("allButtonStates = " + String(allButtonStates));

		// reset sleep counter when buttons are pressed
		if(button1NowPressed || button3NowPressed || button4NowPressed )
		{
			renew_working_timer();
		}
	}

	void ReadSingleButtonState(ButtonStates* buttonState, bool buttonIsNowPressed, long* timeWhenButtonWasPressed)
	{
	//	Serial.println("-- buttonState " + String(*buttonState) );
	//	Serial.println("timeWhenButtonWasPressed " + String(*timeWhenButtonWasPressed) );
	//	Serial.println("buttonIsNowPressed " + String(buttonIsNowPressed) );
	//Serial.println("isButtonStatePressed " + String(isButtonStatePressed) );
	//delay(100);
		if(*buttonState == ButtonState_NoPressed && buttonIsNowPressed == false)
		{
			// do nothing, just ignore it
		}
		else if(*buttonState == ButtonState_NoPressed && buttonIsNowPressed)
		{
			// is pressed for first time
			//Serial.println("press at: " + String(millis()));
			*timeWhenButtonWasPressed = millis();
			*buttonState = ButtonState_Pressed;
			isHandledButtonPress = false;
		}
		else if(*timeWhenButtonWasPressed > 0 && (millis() - *timeWhenButtonWasPressed > LOND_PRESS_TIME) && buttonIsNowPressed)
		{
			// long click detected
			*buttonState = ButtonState_LongPressed;
		}		
		else if(*buttonState == ButtonState_Pressed && buttonIsNowPressed == false)
		{
			// short released
//			Serial.println("short released !!!");
			*timeWhenButtonWasPressed = 0;
			*buttonState = ButtonState_ShortReleased;
		}
		else if(buttonIsNowPressed == false)
		{
			// clear remaining state, for example rease of button after short press or long press
		//	Serial.println("clear btn state!!!");
			*buttonState = ButtonState_NoPressed;
			*timeWhenButtonWasPressed = 0;
		}
	}
};

void setup() {        
	Serial.begin(9600);

	pinMode(PIN_button1, INPUT);    
	pinMode(PIN_button4, INPUT);  
	pinMode(PIN_led, OUTPUT); 
	pinMode(PIN_led_big, OUTPUT); 

	digitalWrite(PIN_button1, HIGH);
	digitalWrite(PIN_button4, HIGH);
	digitalWrite(PIN_button3, HIGH);
	digitalWrite(PIN_led_big, LOW);
}


global globals;

void pin2_isr()
{

}

inline void renew_working_timer()
{
	globals.working_time_ms = millis();
}

void MonitorButtons()
{
	if(globals.isHandledButtonPress == false)
	{
		if(globals.allButtonStates == AllButtonState_Btn1_Btn3_Btn4_Pressed)
		{
			globals.isHandledButtonPress = true;
			Serial.println("$$$ wow - all buttons are pressed !");
			digitalWrite(PIN_led_big, HIGH);delay(1500);digitalWrite(PIN_led_big, LOW);

		}
		if(globals.allButtonStates == AllButtonState_Btn1_Btn4_Pressed)
		{
			globals.isHandledButtonPress = true;
			Serial.println("$$$ reset requested ...");
			delay(20);
			for(int i = 0; i < 10; i++)
			{
		//		digitalWrite(PIN_led_big, HIGH);delay(80);digitalWrite(PIN_led_big, LOW);
			//	delay(80);
			}
		}

		if(globals.allButtonStates == AllButtonState_Btn1_ShortReleased)
		{
			globals.isHandledButtonPress = true;
			Serial.println("$$$ short pressed 1 ...");
			delay(20);
			//digitalWrite(PIN_led_big, HIGH);delay(150);digitalWrite(PIN_led_big, LOW);

		}
		if(globals.allButtonStates == AllButtonState_Btn1_LongPressed  )
		{
			globals.isHandledButtonPress = true;
			Serial.println("$$$ long pressed 1 ...");
			delay(20);
		//	digitalWrite(PIN_led_big, HIGH);delay(400);digitalWrite(PIN_led_big, LOW);
		}

		if(globals.allButtonStates == AllButtonState_Btn3_ShortReleased)
		{
			globals.isHandledButtonPress = true;
			Serial.println("$$$ short pressed 3 ...");
			delay(20);
		//	digitalWrite(PIN_led_big, HIGH);delay(150);digitalWrite(PIN_led_big, LOW);
		//	delay(150);
		//	digitalWrite(PIN_led_big, HIGH);delay(150);digitalWrite(PIN_led_big, LOW);
		//	delay(150);
		//	digitalWrite(PIN_led_big, HIGH);delay(150);digitalWrite(PIN_led_big, LOW);
		}
		if(globals.allButtonStates == AllButtonState_Btn3_LongPressed  )
		{
			globals.isHandledButtonPress = true;
			Serial.println("$$$ long pressed 3 ...");
			delay(20);
		//	digitalWrite(PIN_led_big, HIGH);delay(400);digitalWrite(PIN_led_big, LOW);
		//	delay(400);
		//	digitalWrite(PIN_led_big, HIGH);delay(400);digitalWrite(PIN_led_big, LOW);
		//	delay(400);
		//	digitalWrite(PIN_led_big, HIGH);delay(400);digitalWrite(PIN_led_big, LOW);
		}

		if(globals.allButtonStates == AllButtonState_Btn4_ShortReleased)
		{
			globals.isHandledButtonPress = true;
			Serial.println("$$$ short pressed 4 ...");
			delay(20);
		//	digitalWrite(PIN_led_big, HIGH);delay(150);digitalWrite(PIN_led_big, LOW);
		//	delay(150);
		//	digitalWrite(PIN_led_big, HIGH);delay(150);digitalWrite(PIN_led_big, LOW);
		}
		if(globals.allButtonStates == AllButtonState_Btn4_LongPressed )
		{
			globals.isHandledButtonPress = true;
			Serial.println("$$$ long pressed 4 ...");
			delay(20);
		//	digitalWrite(PIN_led_big, HIGH);delay(400);digitalWrite(PIN_led_big, LOW);
		//	delay(400);
		//	digitalWrite(PIN_led_big, HIGH);delay(400);digitalWrite(PIN_led_big, LOW);
		}
	}

	if(globals.button1State == ButtonState_Pressed)
	{
//		digitalWrite(PIN_led_big, HIGH);
		//delay(1000);
		//digitalWrite(PIN_led_big, LOW);
	}
	else
	{
	//	digitalWrite(PIN_led_big, LOW);
	}

}

void sleepNow() {  
	// turn off LED not to drain power
	digitalWrite(PIN_led_big, LOW);

    sleep_enable();          // enables the sleep bit in the mcucr register  
    attachInterrupt(0,pin2_isr, LOW); // use interrupt 0 (pin 2) and run function  
	attachInterrupt(1,pin2_isr, LOW); // use interrupt 1 (pin 3) and run function  
    set_sleep_mode(SLEEP_MODE_PWR_DOWN);   // sleep mode is set here  
	cli();
	sleep_bod_disable();
	sei();
	sleep_cpu();
	sleep_mode();            // here the device is actually put to sleep!!  

    // THE PROGRAM CONTINUES FROM HERE AFTER WAKING UP  
    sleep_disable();         // first thing after waking from sleep: disable sleep...  
    detachInterrupt(0);      // disables interrupt 0 on pin 2 so the wakeUpNow code will not be executed during normal running time.  
	detachInterrupt(1);

	renew_working_timer();

	// may be it is not needed to read the buttons state here, if sleeping is at the end of loop()
	globals.ReadButtonsState();
}  

void loop() {

	globals.ReadButtonsState();

	MonitorButtons();
	
	bool gotoSleep = false;

	if(/*pin2_interrupt_flag == 1 || */ millis() - globals.working_time_ms > TIME_BEFORE_GOING_TO_SLEEP)
	{
		gotoSleep = true;
		Serial.println("going to sleep ...");
		delay(30);
	}

	if(gotoSleep)
	{
		sleepNow();
		Serial.println("...waking up");
		delay(10);
	}
}
