﻿#include <avr/sleep.h>

const byte PIN_button1 = 2;
const byte PIN_button4 = 3;
const byte PIN_led = 13;

bool button1State = false;
bool button4State = false;

void setup() {        
	pinMode(PIN_button1, INPUT);    
	pinMode(PIN_button4, INPUT);  
	pinMode(PIN_led, OUTPUT); 

	digitalWrite(PIN_button1, HIGH);
	digitalWrite(PIN_button4, HIGH);

	Serial.begin(9600);
}

byte button1;
byte button4;
byte pin2_interrupt_flag = 1;
int working_time_ms = millis();


void pin2_isr()
{
  sleep_disable();
  detachInterrupt(0);
  pin2_interrupt_flag = 0;
  working_time_ms = millis();
}

void loop() {

	bool gotoSleep = false;
	

	if(pin2_interrupt_flag == 1 || millis() - working_time_ms > 1000)
	{
		gotoSleep = true;
		Serial.println(String("going to sleep ..."));
		delay(30);
	}

	if(gotoSleep)
	{
		sleep_enable();
		attachInterrupt(0, pin2_isr, LOW);
		attachInterrupt(1, pin2_isr, LOW);
		/* 0, 1, or many lines of code here */
		set_sleep_mode(SLEEP_MODE_PWR_DOWN);
		cli();
		sleep_bod_disable();
		sei();
		sleep_cpu();
		/* wake up here */
		sleep_disable();
	}

	Serial.println("working");
	button1 = digitalRead(PIN_button1);
	button4 = digitalRead(PIN_button4);
	delay(10);

	if(button1State == false && button1 == LOW)
	{
		button1State = true;
		Serial.println(String("Button 1 is pressed !"));
		digitalWrite(PIN_led, HIGH);
		delay(100);             
		digitalWrite(PIN_led, LOW); 
	}
	if(button1 == HIGH)
	{
		button1State = false;
	}

	if(button4State == false && button4 == LOW)
	{
		button4State = true;
		Serial.println(String("Button 4 is pressed !"));
		digitalWrite(PIN_led, HIGH);
		delay(100);             
		digitalWrite(PIN_led, LOW); 
	}
	if(button4 == HIGH)
	{
		button4State = false;
	}

	delay(20);
}
