#include <EEPROM.h>

#define EEPROM_INDEX_STATISTICS_DATA            ((int) 400)

struct BME280Data
{    
    int temperature;
    int humidity;
    int pressure;
 //   long altitute;
};


struct MeasurementData
{
    BME280Data prevSensorData;        // keep track of the previous two measurements to check if some invalid measurements are detected
//    BME280Data prevPrevSensorData;    // keep track of the previous two measurements to check if some invalid measurements are detected

    int eepromWriteCounter; // count the number of writes we have performed

    int temperature;
    int humidity;
    int pressure;
    long altitute;

    bool upDownFlag;
    long lastRegisteredAltitute;

    int tempDenivelationUp;
    int tempDenivelationDown;
    int maxTempDenivelationUp;
    int maxTempDenivelationDown;
    long totalDenivelationUp;
    long totalDenivelationDown;
    int minTemperature;
    int maxTemperature;
    int minHumidity;
    int maxHumidity;
    int minPressure;
    int maxPressure;
    int minAltitute;
    long maxAltitute;
};

MeasurementData data;

    void saveToEEPROM()
    {
        int init_offset = EEPROM_INDEX_STATISTICS_DATA;
        int size = sizeof(data);
        
//        char temp[10];
  //      (String(size)).toCharArray(temp, 10);
        Serial.print("size=");
        Serial.println(size);

//displayConfirm(size, 2);

        // byte* obj = (byte*) &(this->data);

//        Serial.print("size = ");
//        Serial.println(size);
        for(unsigned int i = 0; i < size; i++)
        {
            if(i == 0)
            {
//displayConfirm(*((int*)(&(this->data) + i)), 2);
//                displayConfirm(i + init_offset, 2);
//displayConfirm(*((byte*)(&(this->data) + i)), 2);
            }

            EEPROM.write(i + init_offset,  *((byte*)(&data + i)));
//            Serial.print(i);Serial.print(" ");
  //          Serial.println(obj[i]);
        }
//displayConfirm(F("4"), 1, NULL, 0);
    };
    void readFromEEPROM()
    {
        int size = sizeof(data);
//        Serial.print("size = ");
//        Serial.println(size);
        //char* obj = (char*) this;
        for(int i = 0; i < size; i++)
        {
//            Serial.print(i);Serial.print(" ");
//            Serial.println(obj[i]);
            ((unsigned char*)&data)[i] = EEPROM.read(i + EEPROM_INDEX_STATISTICS_DATA);
        }

        Serial.println("finished");

        // those two shout be reset to 0
//        lastTimeTemperatureIsMeasuredMs = 0;
        //lastTimeSavedtoEEPROM = 0;
    };



void saveIntToEEPROM(int index, int value)
{
    byte * ptr = (byte*)&value;
    EEPROM.write(index, *ptr);
    EEPROM.write(index + 1, *(ptr + 1));
}

int readIntFromEEPROM(int index)
{
    int result;

    byte* ptr = (byte*) &result;
    *ptr = EEPROM.read(index);
    *(ptr+1) = EEPROM.read(index + 1);

    return result;
}

void saveLongToEEPROM(int index, long value)
{
    byte * ptr = (byte*)&value;
    EEPROM.write(index, *ptr);
    EEPROM.write(index + 1, *(ptr + 1));
    EEPROM.write(index + 2, *(ptr + 2));
    EEPROM.write(index + 3, *(ptr + 3));
}

long readLongFromEEPROM(int index)
{
    long result;

    byte* ptr = (byte*) &result;
    *ptr = EEPROM.read(index);
    *(ptr+1) = EEPROM.read(index + 1);
    *(ptr+2) = EEPROM.read(index + 2);
    *(ptr+3) = EEPROM.read(index + 3);

    return result;
}


// the setup function runs once when you press reset or power the board
void setup() {
    Serial.begin(9600);
    Serial.println("setup");

//    saveIntToEEPROM(4, 12345);
//    readIntFromEEPROM(4);

    saveLongToEEPROM(5, 100345000);
    long x = readLongFromEEPROM(5);
    Serial.println(x);

//    readFromEEPROM();
}

// the loop function runs over and over again until power down or reset
void loop() {
  Serial.println("loop");
  delay(2000);
}


