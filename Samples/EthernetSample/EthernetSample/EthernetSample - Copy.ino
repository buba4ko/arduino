// http://www.tweaking4all.com/hardware/arduino/arduino-enc28j60-ethernet/#ethercard

#include "libraries\ethercard\EtherCard.h"

static byte mymac[] = { 0x40, 0x8D, 0x5C, 0x1E, 0x7F, 0xE7 };
static byte myip[] = { 100,102,0,244 };

byte Ethernet::buffer[700]; 
  
void setup () { 
    Serial.begin(115200);

    Serial.println("111");

  ether.begin(sizeof Ethernet::buffer, mymac,10); 
  ether.staticSetup(myip); 
} 
  
void loop() { 
  
  word len = ether.packetReceive(); 
  word pos = ether.packetLoop(len); 

  Serial.println("start");
  if(pos) {    
    Serial.println("OK");     
    BufferFiller bfill = ether.tcpOffset(); 
    bfill.emit_p(PSTR("<h1>hello</h1>")); 
    ether.httpServerReply(bfill.position()); 
  } 
  else
  {
      Serial.println("fail");
  }
  delay(3000);
}