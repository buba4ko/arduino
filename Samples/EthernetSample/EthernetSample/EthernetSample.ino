// This is a demo of the RBBB running as webserver with the Ether Card
// 2010-05-28 <jc@wippler.nl> http://opensource.org/licenses/mit-license.php


#include "libraries\ethercard\EtherCard.h"

// ethernet interface mac address, must be unique on the LAN
static byte mymac[] = { 0x40, 0x8D, 0x5C, 0x2E, 0x7F, 0xE8 };

static byte myip[] = { 100,102,0,66 };

byte Ethernet::buffer[500];
BufferFiller bfill;

void setup () {
        Serial.begin(115200);

    Serial.println("111");
    delay(30);
  if (ether.begin(sizeof Ethernet::buffer, mymac) == 0)
  {
    Serial.println( "Failed to access Ethernet controller");
    Serial.println("000");
  }
  Serial.println("222");
  delay(30);
  ether.staticSetup(myip);
  Serial.println("333");
  delay(30);
}

static word homePage() {
  long t = millis() / 1000;
  word h = t / 3600;
  byte m = (t / 60) % 60;
  byte s = t % 60;
  bfill = ether.tcpOffset();
  bfill.emit_p(PSTR(
    "HTTP/1.0 200 OK\r\n"
    "Content-Type: text/html\r\n"
    "Pragma: no-cache\r\n"
    "\r\n"
    "<meta http-equiv='refresh' content='1'/>"
    "<title>RBBB server</title>"
    "<h1>$D$D:$D$D:$D$D</h1>"),
      h/10, h%10, m/10, m%10, s/10, s%10);
  return bfill.position();
}

void loop () {

   // Serial.println("222");

  word len = ether.packetReceive();
  word pos = ether.packetLoop(len);
  
  
 // Serial.println("start");

  if (pos)  // check if valid tcp data is received
    ether.httpServerReply(homePage()); // send web page data
}