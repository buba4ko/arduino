#include <ESP8266WiFi.h>

const char* ssid = "sbnd";
const char* password = "|_s_b_n_d_|";

// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(80);

void setup() {
  Serial.begin(115200);
  delay(10);

  // prepare GPIO2
  pinMode(2, OUTPUT);
  digitalWrite(2, 0);

  WiFi.disconnect();
  delay(200);
  
  // Connect to WiFi network
  Serial.println();
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  WiFi.begin(ssid, password);
  
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  
  // Start the server
  server.begin();
  Serial.println("Server started");

  // Print the IP address
  Serial.println(WiFi.localIP());
}

void HelloArduinoTeam(WiFiClient& client)
{
	long mils = millis();
	int mil = mils % 1000;
	int sec = (mils / 1000) % 60;
	int min = (mils / (60*1000)) % 60;
	int hours = (mils / (60*60*1000)) % 60;
  String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\nHello Arduino Team ";
  s += "\r\n<br/>" + String(hours) + " hours " + min + " mins " + sec + " sec " + mil + " ms";
  s += "</html>\n";
  client.print(s);
  delay(3);
  Serial.println("Client disonnected");
  client.stop();
}

WiFiClient client;
void loop() {

    if(WiFi.status() != WL_CONNECTED) {
        Serial.println("[loop] no wifi");
        delay(1000);
    }

  // Check if a client has connected
  client = server.available();
  if (!client) {
	//  delay(10);
    return;
  }
  
  // Wait until the client sends some data
  Serial.println();
  Serial.println("new client");
  delay(3);
  long counter = 0;
  while(!client.available()){
	counter++;
    delay(3);
	if(counter > (long) 10*1000)
	{
		Serial.println("endless loop detected");
		break;
	}
  }
  
  // Read the first line of the request
  String req = client.readStringUntil('\r');
  Serial.println(req);
  client.flush();
  
  // Match the request
  int val;
  if (req.indexOf("/gpio/0") != -1)
    val = 0;
  else if (req.indexOf("/gpio/1") != -1)
    val = 1;
  else if (req.indexOf("/favicon.ico") != -1)
  {
	  // ignore the favicon.ico request
	  Serial.println("ignoring the favicon.ico");
	  delay(3);
	  return;
  }
  else {
	  client.flush();
	  HelloArduinoTeam(client);
    //Serial.println("invalid request");
    //client.stop();
    return;
  }

  // Set GPIO2 according to the request
  digitalWrite(2, val);
  
  client.flush();

  // Prepare the response
  String s = "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n<!DOCTYPE HTML>\r\n<html>\r\nGPIO is now ";
  s += (val)?"high":"low";
  s += "</html>\n";

  // Send the response to the client
  client.print(s);
  delay(1);
  Serial.println("Client disonnected");
  client.stop();

  // The client will actually be disconnected 
  // when the function returns and 'client' object is detroyed
}
