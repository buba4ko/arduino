﻿#include <SoftwareSerial.h>
#include <Wire.h>


#include "libraries\SSCI_BME280\SSCI_BME280_new.h"


BME280_2 bme280_2;

void setup()
{
 
	Serial.begin(9600);
	Wire.begin();

  bme280_2.setup();
}


void loop()
{
  bme280_2.loop2();

  delay(1000);
}
