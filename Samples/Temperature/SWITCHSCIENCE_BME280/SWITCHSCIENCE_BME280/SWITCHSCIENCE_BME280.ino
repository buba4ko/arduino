﻿#include <Wire.h>

#include "libraries\SSCI_BME280\SSCI_BME280.h"
#include "libraries\SSCI_BME280\SSCI_BME280_new.h"

SSCI_BME280 bme280;

void setup()
{
  uint8_t osrs_t = 1;             //Temperature oversampling x 1
  uint8_t osrs_p = 1;             //Pressure oversampling x 1
  uint8_t osrs_h = 1;             //Humidity oversampling x 1
  uint8_t bme280mode = 3;         //Normal mode
  uint8_t t_sb = 5;               //Tstandby 1000ms
  uint8_t filter = 0;             //Filter off
  uint8_t spi3w_en = 0;           //3-wire SPI Disable

  Serial.begin(115200);
  Wire.begin();
  bme280.setMode(osrs_t, osrs_p, osrs_h, bme280mode, t_sb, filter, spi3w_en);

  bme280.readTrim();
}


void loop()
{
  double temp_act, press_act, hum_act; //最終的に表示される値を入れる変数

  bme280.readData(&temp_act, &press_act, &hum_act);

  Serial.print("TEMP : ");
  Serial.print(temp_act);
  Serial.println(" DegC  ");
  Serial.print("PRESS : ");
  Serial.print(press_act);
  Serial.println(" hPa  ");
  Serial.print("HUM : ");
  Serial.println(hum_act);
  Serial.println(" ");

  delay(3000);
}
