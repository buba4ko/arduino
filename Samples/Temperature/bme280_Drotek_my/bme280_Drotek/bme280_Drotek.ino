#include <Wire.h>
#include <SPI.h>
#include "libraries\BME280-master\Sensor_Master.h"
#include "libraries\BME280-master\BME280.h"

/*******************************************************************************************
This is an exemple for the BMP280 library 
Drotek BMP280 breakout board can be found here:
http://www.drotek.com/shop/en/home/751-bmp280-breakout-board.html
Please follow the 3 step configuration below.
*******************************************************************************************/

//////////////////////////////////Configuration//////////////////////////////////////////////
                                                                                           //
//First step, select your arduino board type by incomment the right line//                 //
//#define Arduino_UNO                                                                      //
//#define Arduino_MEGA2560                                                                   //                                                                
/////////////////////////////////////////////////////////////////////////////////////////////                                                            


#define SEALEVELPRESSURE_HPA (1013.25)

void setup() {
  Serial.begin(9600);
}

// here is how we can use it
void GetBMECurrentData(float& temperature, float& pressure, float& altitute, float& humidity)
{
	Adafruit_BME280 bme;

	if (!bme.begin()) {
	//	Serial.println("Could not find a valid BME280 sensor, check wiring!");
		while (1);
	}
    temperature = bme.readTemperature();
	pressure = bme.readPressure() / 100.0F;
    altitute = bme.readAltitude(SEALEVELPRESSURE_HPA);
	humidity = bme.readHumidity();
}

void loop() {
	float temperature;
	float pressure;
	float altitute;
	float humidity;

	long mils = millis();
	GetBMECurrentData(temperature, pressure, altitute, humidity);
	long mils2 = millis();

	Serial.print("time to read data = ");
	Serial.println(mils2 - mils);

    Serial.print("Temperature = ");
    Serial.print(temperature);
    Serial.println(" *C");

    Serial.print("Pressure = ");
    Serial.print(pressure);
    Serial.println(" hPa");

    Serial.print("Approx. Altitude = ");
    Serial.print(altitute);
    Serial.println(" m");

    Serial.print("Humidity = ");
    Serial.print(humidity);
    Serial.println(" %");

    Serial.println();
    delay(4000);
}

