#include <LowPower.h>

#define PIN_led 13

void setup() {
    Serial.begin(9600);

    pinMode(PIN_led, OUTPUT);
}

// the loop function runs over and over again until power down or reset
void loop() {

    Serial.println(millis());

    digitalWrite(13, HIGH);
    delay(1000);
    digitalWrite(13, LOW);
    delay(1000);

    //LowPower.powerDown(SLEEP_FOREVER, ADC_OFF, BOD_OFF); 

}
